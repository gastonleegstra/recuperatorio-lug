﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Proceso
    {
        private int id_proceso;

        public int ID_Proceso
        {
            get { return id_proceso; }
            set { id_proceso = value; }
        }

        private string nombre_proceso;

        public string Nombre_Proceso
        {
            get { return nombre_proceso; }
            set { nombre_proceso = value; }
        }

        private string tiempo_ejecucion;

        public string Tiempo_Ejecucion
        {
            set { tiempo_ejecucion = value; }
            get { return tiempo_ejecucion; }
        }



    }
}
