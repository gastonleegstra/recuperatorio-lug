﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.ComponentModel;
using BE;

namespace DAL
{
    public class MP_Proceso
    {
        private Acceso acceso = new Acceso();
        private List<BE.Proceso> ejecucion = new List<BE.Proceso>();
        public List<BE.Proceso> Procesos_En_Ejecucion()
        {
            BE.Proceso proceso;
            ejecucion.Clear();
            Process[] enlinea = Process.GetProcesses();
            DateTime time = DateTime.Now;
            TimeSpan span;
            foreach (Process item in enlinea)
            {
                proceso = new BE.Proceso();
                proceso.ID_Proceso = item.Id;
                proceso.Nombre_Proceso = item.ProcessName.ToString();
                try
                {
                    span = time - item.StartTime;
                    proceso.Tiempo_Ejecucion = span.Days.ToString() + " Dias" + ", " + span.Hours.ToString("00") + ":" + span.Minutes.ToString("00") + ":" + span.Seconds.ToString("00");
                }
                catch (Win32Exception)
                {
                    proceso.Tiempo_Ejecucion = "Acceso Denegado";
                }
 
                ejecucion.Add(proceso);
            }
            return ejecucion.OrderBy(p=>p.ID_Proceso).ToList();
        }

        public List<Proceso> Leer_XML()
        {
            List<BE.Proceso> procesos = new List<Proceso>();
            XmlDocument document = new XmlDocument();
            string path = Directory.GetCurrentDirectory() + @"\Procesos.xml";
            IEnumerable<XElement> procesosxml = XDocument.Load(path).Element("Procesos").Descendants("Proceso");
            foreach (XElement procesoxml in procesosxml)
            {
                BE.Proceso proceso = new BE.Proceso();

                proceso.ID_Proceso = int.Parse(procesoxml.Element("Id_Proceso").Value.ToString());
                proceso.Nombre_Proceso = procesoxml.Element("Nombre").Value.ToString();
                proceso.Tiempo_Ejecucion = procesoxml.Element("Tiempo_Ejecucion").Value.ToString();

                procesos.Add(proceso);
            }

            return procesos;
        }

        public void Guardar_XML(List<Proceso> procesos)
        {
            XmlDocument document = new XmlDocument();
            XmlNode raiz = document.CreateElement("Procesos");
            document.AppendChild(raiz);
            foreach (BE.Proceso item in procesos)
            {
                XmlNode proceso = document.CreateElement("Proceso");
                    XmlNode id = document.CreateElement("Id_Proceso");
                    id.InnerText = item.ID_Proceso.ToString();
                    proceso.AppendChild(id);
                    XmlNode nombre = document.CreateElement("Nombre");
                    nombre.InnerText = item.Nombre_Proceso.ToString();
                    proceso.AppendChild(nombre);
                    XmlNode tiempo = document.CreateElement("Tiempo_Ejecucion");
                    tiempo.InnerText = item.Tiempo_Ejecucion.ToString();
                    proceso.AppendChild(tiempo);
                raiz.AppendChild(proceso);
            }
            string path = Directory.GetCurrentDirectory() + @"\Procesos.xml";
            document.Save(path);
        }

        public void Guardar_BBDD(List<Proceso> procesos)
        {

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            foreach (BE.Proceso proceso in procesos)
            {
                List<BE.Proceso> guardados = Leer_BBDD();
                parameters.Clear();
                parameters.Add(acceso.CrearParametro("@id", proceso.ID_Proceso));
                parameters.Add(acceso.CrearParametro("@nombre", proceso.Nombre_Proceso));
                parameters.Add(acceso.CrearParametro("@tiempo_ejecuccion", proceso.Tiempo_Ejecucion));
                if (guardados.Exists(p=>p.ID_Proceso==proceso.ID_Proceso))
                {
                    acceso.Abrir();
                    acceso.Escribir("PROCESO_EDITAR", parameters);
                    acceso.Cerrar();
                }
                else
                {
                    acceso.Abrir();
                    acceso.Escribir("PROCESO_INSERTAR", parameters);
                    acceso.Cerrar();
                } 
            }
            
            
        }
        public List<BE.Proceso> Leer_BBDD()
        {
            BE.Proceso proceso;
            List<BE.Proceso> procesos = new List<Proceso>();
            DataTable table = new DataTable();
            acceso.Abrir();
            table = acceso.Leer("PROCESO_LEER");
            acceso.Cerrar();
            foreach (DataRow row in table.Rows)
            {
                proceso = new BE.Proceso();
                proceso.ID_Proceso = int.Parse(row[0].ToString());
                proceso.Nombre_Proceso = row[1].ToString();
                proceso.Tiempo_Ejecucion = row[2].ToString();
                procesos.Add(proceso);
            }
            return procesos.OrderBy(p => p.ID_Proceso).ToList();
        }

    }
}
