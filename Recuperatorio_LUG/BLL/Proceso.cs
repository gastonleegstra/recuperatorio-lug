﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace BLL
{
    public class Procesos
    {

        DAL.MP_Proceso mp_proceso = new DAL.MP_Proceso();

        public List<BE.Proceso> Procesos_En_Ejecucion()
        {
            return mp_proceso.Procesos_En_Ejecucion();
        }

        public int Abrir_Aplicacion(string app)
        {
            int result = 1;
            try
            {
                Process.Start(app);
            }
            catch 
            {

                result = -1;
            }
            return result;
        }
        public int Abrir_Aplicacion(string app, string parametros)
        {
            int result = 1;
            try
            {
                Process.Start(app, parametros);
            }
            catch 
            {
                result = -1;
            }
            return result;
        }

        public void Guardar_BBDD()
        {
                mp_proceso.Guardar_BBDD(Procesos_En_Ejecucion());
        }

        public void Guardar_XML()
        {
            mp_proceso.Guardar_XML(Procesos_En_Ejecucion());
        }

        public List<BE.Proceso> Leer_BBDD()
        {
            return mp_proceso.Leer_BBDD();
        }

        public List<BE.Proceso> Leer_XML()
        {
            return mp_proceso.Leer_XML();
        }

    }
}
