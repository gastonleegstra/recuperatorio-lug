﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procesos
{
    public partial class Frm_Abrir_Aplicacion : Form
    {
        BLL.Procesos proceso_gestor = new BLL.Procesos();
        public Frm_Abrir_Aplicacion()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int result = 1;
            if (radioButton1.Checked==true)
            {
                if (!(string.IsNullOrEmpty(textBox3.Text))||!(string.IsNullOrWhiteSpace(textBox3.Text)))
                {
                   result = proceso_gestor.Abrir_Aplicacion(textBox3.Text);
                }
            }
            else
            {
                if ((!(string.IsNullOrEmpty(textBox1.Text)) || !(string.IsNullOrWhiteSpace(textBox1.Text))) && (!(string.IsNullOrEmpty(textBox2.Text)) || !(string.IsNullOrWhiteSpace(textBox2.Text))))
                {
                    string param = '"' + textBox2.Text + '"'; 
                    result = proceso_gestor.Abrir_Aplicacion(textBox1.Text,param);
                }
            }
            if (result == -1)
            {
                MessageBox.Show("Upss!!! algo mal sucedio al intentar abrir la aplicacion");
            }
            Close();
        }

        private void Mostrar() 
        {
            if (radioButton1.Checked == true)
            {
                groupBox1.Visible = false;
                groupBox1.Enabled = false;
                groupBox2.Visible = true;
                groupBox2.Enabled = true;
            }
            else
            {
                groupBox1.Visible = true;
                groupBox1.Enabled = true;
                groupBox2.Visible = false;
                groupBox2.Enabled = false;
            }
        }
        private void Frm_Abrir_Aplicacion_Load(object sender, EventArgs e)
        {
            Mostrar();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Mostrar();
        }
    }
}
