﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Procesos
{
    public partial class Frm_Procesos : Form
    {
        BLL.Procesos gestor_proceso = new BLL.Procesos();
        Frm_Abrir_Aplicacion frm_abrir_app;
        public Frm_Procesos()
        {
            InitializeComponent();
        }

        private void Enlazar() 
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestor_proceso.Procesos_En_Ejecucion();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (frm_abrir_app==null)
            {
                frm_abrir_app = new Frm_Abrir_Aplicacion();
                frm_abrir_app.FormClosed += Frm_abrir_app_FormClosed;
                frm_abrir_app.ShowDialog();
            }
        }

        private void Frm_abrir_app_FormClosed(object sender, FormClosedEventArgs e)
        {
            frm_abrir_app = null;
            Enlazar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            gestor_proceso.Guardar_BBDD();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            gestor_proceso.Guardar_XML();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = gestor_proceso.Leer_BBDD();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            dataGridView3.DataSource = null;
            dataGridView3.DataSource = gestor_proceso.Leer_XML();
        }
    }
}
