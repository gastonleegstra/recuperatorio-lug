USE [Recuperatorio_LUG]
GO
/****** Object:  Table [dbo].[Proceso]    Script Date: 19/11/2020 23:06:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proceso](
	[id_proceso] [int] NULL,
	[nombre] [nvarchar](50) NULL,
	[tiempo] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[PROCESO_EDITAR]    Script Date: 19/11/2020 23:06:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PROCESO_EDITAR]
@id int,
@nombre nvarchar(50),
@tiempo_ejecuccion nvarchar(50)
as
Begin
update Proceso set nombre=@nombre, tiempo=@tiempo_ejecuccion where id_proceso = @id
end 
GO
/****** Object:  StoredProcedure [dbo].[PROCESO_INSERTAR]    Script Date: 19/11/2020 23:06:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PROCESO_INSERTAR]
@id int,
@nombre nvarchar(50),
@tiempo_ejecuccion nvarchar(50)
as
Begin
insert into Proceso values (@id,@nombre,@tiempo_ejecuccion)
end 
GO
/****** Object:  StoredProcedure [dbo].[PROCESO_LEER]    Script Date: 19/11/2020 23:06:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PROCESO_LEER]
as
Begin
select * from Proceso
end 
GO
